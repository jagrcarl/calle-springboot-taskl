package se.experis.SpringBootHello.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    /*
    * GetMapping makes a HTTP GET request, for either /greeting or /reverse.
    * Requestparam comes from the html code in index => name="input in input and add it to String input
    * &#x202e; is HTML-code and will return the string backwards
     */

    @GetMapping(value = "/greeting")
    public String greeting (@RequestParam("input") String input){
        return "Hello " + input;
    }

    @GetMapping(value = "/reverse")
    public String reverse (@RequestParam("input") String input){
        return "&#x202e;" + input;
    }
}
